class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :name
      t.string :email

      t.timestamps # создаёт created_at и updated_at, метки времени
    end
  end
end
