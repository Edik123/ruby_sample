require 'test_helper'

class UsersLayoutTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:michael)
  end
  
  test "users layout links" do
    log_in_as(@user)
    get users_path
    assert_template 'users/index'
    #assert_select "a[href=?]", root_path, count: 2
    #assert_select "a[href=?]", help_path
    #assert_select "a[href=?]", about_path
    #assert_select "a[href=?]", contact_path
  end
end
