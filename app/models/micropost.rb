class Micropost < ApplicationRecord
  belongs_to :user
  mount_uploader :picture, PictureUploader    # связываем изображение с моделью
  default_scope -> { order(created_at: :desc) } # сортировка постов по убыванию
  validates :content, presence: true, length: { maximum: 140 }
  validates :user_id, presence: true
  validate  :picture_size
  
private

  # Проверяет размер загруженного изображения
  def picture_size
    if picture.size > 5.megabytes
      errors.add(:picture, "should be less than 5MB")
    end
  end
end
