class User < ApplicationRecord
  has_many :microposts, dependent: :destroy # удаляются посты если удалить юзера
  has_many :active_relationships, class_name:  "Relationship",
                                  foreign_key: "follower_id",
                                  dependent:   :destroy
  has_many :passive_relationships, class_name:  "Relationship",
                                   foreign_key: "followed_id",
                                   dependent:   :destroy
  # источником following массива является набор followed идентификаторов
  has_many :following, through: :active_relationships, source: :followed
  has_many :followers, through: :passive_relationships, source: :follower
  
  attr_accessor :remember_token, :activation_token, :reset_token  # getter и setter
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  before_save { email.downcase! }  # или { self.email = email.downcase }
  before_create :create_activation_digest # это действие происходит до создания пользователя
  
  validates :name, presence: true, length: { maximum: 50 }
  validates :email, presence: true, length: { maximum: 250 }, 
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false } # учитывает регистр
  has_secure_password   # хеширует пароль
  validates :password, presence: true, length: { minimum: 6 }, allow_nil: true
  
  # Returns the hash digest of the given string
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end
  
  # Returns a random token.
  def User.new_token
    SecureRandom.urlsafe_base64
  end
  
  # Запоминает пользователя в базе данных для использования в постоянных сеансах
  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end
  
  # Возвращает true, если данный токен соответствует дайджесту
  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end
  
  # Активирует учетную запись
  def activate
    update_columns(activated: true, activated_at: Time.zone.now)
  end
  
  # Отправляет активацию на электронную почту
  def send_activation_email
    UserMailer.account_activation(self).deliver_now
  end
  
  # Устанавливает атрибуты сброса пароля
  def create_reset_digest
    self.reset_token = User.new_token
    update_columns(reset_digest: User.digest(reset_token), reset_sent_at: Time.zone.now)
  end
  
  # Отправляет письмо для сброса пароля
  def send_password_reset_email
    UserMailer.password_reset(self).deliver_now
  end
  
  # Возвращает true, если срок действия пароля истек
  def password_reset_expired?
    reset_sent_at < 2.hours.ago
  end
  
  # Получаем свои микропосты и тех на кого мы подписанны
  def feed
    # following_ids == following.map(&:id)
    following_ids = "SELECT followed_id FROM relationships
                     WHERE  follower_id = :user_id"
    Micropost.where("user_id IN (#{following_ids}) OR user_id = :user_id", user_id: id) 
    # ? в выражении гарантирует, что id будет правильно экранирован, 
    #   прежде чем будет включен в SQL-запрос, избегая SQL-инъекции
  end
  
  # Подписаться на пользователя, << присоединяет к концу массива
  def follow(other_user)
    following << other_user
  end
  
  # Отписаться от пользователя
  def unfollow(other_user)
    following.delete(other_user)
  end
  
  # Возвращает true, если текущий пользователь подписан на другого пользователя
  def following?(other_user)
    following.include?(other_user)
  end
  
  # Забывает пользователя
  def forget
    update_attribute(:remember_digest, nil)
  end

private
  
  # Создает и назначает токен активации и дайджест
  # Когда происходит метод save юзера, эти данные тож заносятся
  def create_activation_digest
      self.activation_token  = User.new_token
      self.activation_digest = User.digest(activation_token)
  end
end
