class UsersController < ApplicationController
  # требует регистрацию перед использованием методов edit и update, иначе
  # выводит ошибку и перенаправляет на форму регистрации методом logged_in_user
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy, 
                                        :following, :followers]
  before_action :correct_user,   only: [:edit, :update]
  before_action :admin_user,     only: :destroy
  
  def index
    # params[:page] генерируется автоматически will_paginate(по умолчанию 30)
    @users = User.where(activated: true).paginate(page: params[:page])
  end
  
  # Форма регистрации
  def new
    @user = User.new
  end
  
  def show
    @user = User.find(params[:id])
    @microposts = @user.microposts.paginate(page: params[:page])
    # Протестить строку ниже, также написать тесты для index /users и
    # show /user/id! При переходе на стр не активированного
    # юзера, происходит перенаправление на главную 
    #redirect_to root_url and return unless @user.activated?
    #debugger
  end
  
  def create
    @user = User.new(user_params)
    if @user.save
      @user.send_activation_email
      flash[:info] = "Please check your email to activate your account."
      redirect_to root_url
    else
      render 'new'
    end
  end
  
  # Форма редактирования
  def edit
    @user = User.find(params[:id])
  end
  
  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "Profile updated"
      redirect_to @user  # аналогично redirect_to user_url(@user)
    else
      render 'edit'
    end
  end
  
  # Удаление пользователя
  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to users_url
  end
  
  def following
    @title = "Following"
    @user  = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end

  def followers
    @title = "Followers"
    @user  = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end
  
  private
     # Безопасная передача параметров методом private
    def user_params
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation)
    end
  
    # Подтверждает правильного пользователя
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
  
    # Подтверждает администратора
    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end
end
