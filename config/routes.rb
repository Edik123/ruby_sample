Rails.application.routes.draw do

  root 'static_pages#home'
  get  '/home' => 'static_pages#home'
  get  '/help' => 'static_pages#help'
  get  '/about' => 'static_pages#about'
  get  '/contact' => 'static_pages#contact'
  
  get  '/signup' => 'users#new'  # Форма регистрации
  post '/signup' => 'users#create'
  
  get    '/login' => 'sessions#new' # Форма входа
  post   '/login' => 'sessions#create'
  delete '/logout' => 'sessions#destroy'
  resources :users   # RESTful Users
  resources :users do
    member do
      get :following, :followers
    end
  end
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  resources :microposts,          only: [:create, :destroy]
  resources :relationships,       only: [:create, :destroy]
  
  # get    /users        index   users_path
  # get    /users/1      show    user_path(user)
  # get    /users/new    new     new_user_path
  # post   /users        create  users_path
  # get    /users/1/edit edit    edit_user_path(user)
  # patch  /users/1      update  user_path(user)
  # delete /users/1      destroy user_path(user)
  
  # GET	/users/1/following	following	following_user_path(1)
  # GET	/users/1/followers	followers	followers_user_path(1)
  
  # GET	http://ex.co/account_activation/<token>/edit edit	edit_account_activation_url(token)

  # GET	  /password_resets/new	                  new	    new_password_reset_path
  # POST	/password_resets	                      create	password_resets_path
  # GET	http://ex.co/password_resets/<token>/edit	edit	  edit_password_reset_url(token)
  # PATCH	/password_resets/<token>	              update	password_reset_path(token)

  # POST	  /microposts	  create	microposts_path
  # DELETE	/microposts/1	destroy	micropost_path(micropost)
end
